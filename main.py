from rich.console import Console
from prompt import prompt
import os
import getpass
try:
	import signal
	def handler(signum, frame):
		console.set_alt_screen(False)

	signal.signal(signal.SIGTSTP, handler)
except:
	pass

console = Console(highlight = False)

console.set_alt_screen(True)


try:
	print = console.print
	
	
	console.rule('Fi[grey]le[white]Ma[grey]nager')
	print("[green]Welcome to FiMa. This is the default prompt. Type exit to leave, ctrl-z to return to shell (fg to come back)")
	
	while True:
		print(prompt(os.getcwd(), getpass.getuser()), end=" ")
		command = console.input()

except KeyboardInterrupt:
	print('\nGot ctrl-c from user, exitting')
finally:
	console.set_alt_screen(False)
