from rich.tree import Tree
from pathlib import Path
import os

def shorten_path(file_path, length):
	return Path(*Path(file_path).parts[-length:])

class prompt():
	def __init__(self, dir, user):
		self.directory = dir
		self.user = user
	def __rich_console__(self, console, opts):
		dir = shorten_path(self.directory, 3)
		if dir != self.directory:
			dir = '...' + str(dir)
		yield f"[purple]{self.user} [green] @ [blue]{dir} [green]➤"
	
